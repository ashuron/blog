class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :post_id
      t.string :integer
      t.string :body
      t.string :text

      t.timestamps
    end
  end
end
